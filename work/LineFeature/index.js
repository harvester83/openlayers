import Feature from 'ol/Feature.js';
import Map from 'ol/Map.js';
import View from 'ol/View.js';
import { LineString } from 'ol/geom.js';
import OSM from 'ol/source/OSM';
import TileLayer from 'ol/layer/Tile';
import { Vector as VectorLayer } from 'ol/layer.js';
import { Vector as VectorSource } from 'ol/source.js';
import { toStringXY } from 'ol/coordinate';
import { Stroke, Style } from 'ol/style.js';

var lineFeature = new Feature(
    new LineString([[-1e7, 1e6], [-1e6, 3e6]])
    // new LineString(toStringXY([7.85, 47.983333], [8.85, 48.983333]))
);

var map = new Map({
  layers: [
    new TileLayer({
      source: new OSM()
    }),
    new VectorLayer({
      source: new VectorSource({
        features: [lineFeature],
      }),
      style: new Style({
        stroke: new Stroke({
          width: 5,
          color: [255, 0, 0, 1],
        }),
      }),
    }),
    new VectorLayer({
      source: new VectorSource({ wrapX: false }),
    }),
  ],
  target: 'map',
  view: new View({
    center: [0, 0],
    zoom: 2,
  }),
});
