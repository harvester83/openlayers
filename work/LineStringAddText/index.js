import Map from 'ol/Map.js';
import View from 'ol/View.js';
import Point from 'ol/geom/Point.js';
import Draw from 'ol/interaction/Draw.js';
import {Tile as TileLayer, Vector as VectorLayer} from 'ol/layer.js';
import {OSM, Vector as VectorSource} from 'ol/source.js';
import {Fill, Stroke, Style} from 'ol/style.js';

var raster = new TileLayer({
  source: new OSM()
});

var source = new VectorSource();

function styleFunc(feature) {
  var styles = [
    new Style({
      stroke: new Stroke({
        color: "#ffcc33",
        width: 2
      })
    })
  ];
  var extent = feature.getGeometry().getExtent();
  var topRightCoor = [extent[2], extent[3]];
  styles.push(new Style({
    geometry: new Point(topRightCoor),
    text: new Text({
      text: feature.get('QUALIF') + ' ' + feature.get('DENOM'),
      font: '12px Calibri,sans-serif',
      fill: new Fill({
        color: 'black'
      }),
      //stroke: new ol.style.Stroke({color: 'white', width: 1}),
      overflow: true,
      backgroundFill: new Fill({
        color: 'orange'
      }),
      placement: "point",
      textBaseline: "top"
    })
  }));
  return styles;
}

var vector = new ol.layer.Vector({
  source: vectorSource,
  style:styleFunc,
  declutter: true
});



var map = new Map({
  layers: [raster, vector],
  target: 'map',
  view: new View({
    center: [-11000000, 4600000],
    zoom: 4
  })
});

map.addInteraction(new Draw({
  source: source,
  type: 'LineString'
}));