import Map from 'ol/Map.js';
import View from 'ol/View.js';
import Draw from 'ol/interaction/Draw.js';
import {Tile as TileLayer, Vector as VectorLayer} from 'ol/layer.js';
import {OSM, Vector as VectorSource} from 'ol/source.js';

var raster = new TileLayer({
  source: new OSM()
});

var source = new VectorSource({wrapX: false});

var vector = new VectorLayer({
  source: source
});

var map = new Map({
  layers: [raster, vector],
  target: 'map',
  view: new View({
    center: [-11000000, 4600000],
    zoom: 4
  })
});

var button = document.getElementById('point');
console.log(button);

var draw;

function addInteraction() {
  var value = button.value;
  if (value !== 'None') {
    draw = new Draw({
      source: source,
      type: 'Point'
    });

    map.addInteraction(draw);
  }
}

button.addEventListener('click', function () {
  map.removeInteraction(draw);
  addInteraction();
});

addInteraction();