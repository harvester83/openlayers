import Map from 'ol/Map.js';
import View from 'ol/View.js';
import Draw from 'ol/interaction/Draw.js';
//import {defaults as defaultInteractions, Pointer as PointerInteraction} from 'ol/interaction.js';
import {Pointer} from 'ol/interaction.js';
import {Tile as TileLayer, Vector as VectorLayer} from 'ol/layer.js';
import {OSM, Vector as VectorSource} from 'ol/source.js';

const raster = new TileLayer({
    source: new OSM()
});

const source = new VectorSource({wrapX: false});

const vector = new VectorLayer({
    source: source
});

const map = new Map({
    layers: [raster, vector],
    target: 'map',
    view: new View({
        center: [-11000000, 4600000],
        zoom: 4
    })
});

const pointer = new Pointer({});
const mapr = new Map({});
const drawr = new Draw({});

console.log( pointer);
console.log( mapr );
console.log( drawr );

const typeSelect = document.getElementById('type');
let draw; // global so we can remove it later
function addInteraction() {
    let value = typeSelect.value;
    if (value !== 'None') {
        draw = new Draw({
            source: source,
            type: typeSelect.value
        });

        console.log(draw);
        map.addInteraction(draw);
    }
}

/**
 * Handle change event.
 */
typeSelect.onchange = function() {
    map.removeInteraction(draw);
    addInteraction();
};

addInteraction();