const parcelEdgesLayer = new ol.layer.Vector({
    source: this.parcelEdgeSource,
    style: (feature, resolution) => {
        const length = feature.getProperties().length as number;
        const text = ''; // (feature.getGeometry() as ol.geom.LineString).getLength().toFixed(2);    // <<<<<<<<<<<
        const baseline = feature.getProperties().baseline as string;
        return new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: 'rgba(0, 0, 0, 1.0)',
                width: 3
            }),
            text: new ol.style.Text({
                placement: 'line',

                stroke: new ol.style.Stroke({
                    color: 'rgba(255,255,255, 1.0)',
                    width: 3
                }),
                scale: 1.25 * this.fontSize / 6,
                text,
                textBaseline: baseline
            })
        });
    }
});