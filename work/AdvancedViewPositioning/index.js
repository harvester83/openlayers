import Map from 'ol/Map.js';
import View from 'ol/View.js';
import GeoJSON from 'ol/format/GeoJSON.js';
import {Tile as TileLayer, Vector as VectorLayer} from 'ol/layer.js';
import {OSM, Vector as VectorSource} from 'ol/source.js';
import {Circle as CircleStyle, Fill, Stroke, Style} from 'ol/style.js';

var source = new VectorSource({
  url: 'data/geojson/switzerland.geojson',
  format: new GeoJSON()
});
var style = new Style({
  fill: new Fill({
    color: 'rgba(255, 255, 255, 0.6)'
  }),
  stroke: new Stroke({
    color: '#319FD3',
    width: 1
  }),
  image: new CircleStyle({
    radius: 5,
    fill: new Fill({
      color: 'rgba(255, 255, 255, 0.6)'
    }),
    stroke: new Stroke({
      color: '#319FD3',
      width: 1
    })
  })
});
var vectorLayer = new VectorLayer({
  source: source,
  style: style
});
var view = new View({
  center: [0, 0],
  zoom: 1
});
var map = new Map({
  layers: [
    new TileLayer({
      source: new OSM()
    }),
    vectorLayer
  ],
  target: 'map',
  view: view
});

var zoomtoswitzerlandbest = document.getElementById('zoomtoswitzerlandbest');
zoomtoswitzerlandbest.addEventListener('click', function() {
  var feature = source.getFeatures()[0];
  var polygon = /** @type {module:ol/geom/SimpleGeometry~SimpleGeometry} */ (feature.getGeometry());
  view.fit(polygon, {padding: [170, 50, 30, 150], constrainResolution: false});
}, false);

var zoomtoswitzerlandconstrained =
    document.getElementById('zoomtoswitzerlandconstrained');
zoomtoswitzerlandconstrained.addEventListener('click', function() {
  var feature = source.getFeatures()[0];
  var polygon = /** @type {module:ol/geom/SimpleGeometry~SimpleGeometry} */ (feature.getGeometry());
  view.fit(polygon, {padding: [170, 50, 30, 150]});
}, false);

var zoomtoswitzerlandnearest =
    document.getElementById('zoomtoswitzerlandnearest');
zoomtoswitzerlandnearest.addEventListener('click', function() {
  var feature = source.getFeatures()[0];
  var polygon = /** @type {module:ol/geom/SimpleGeometry~SimpleGeometry} */ (feature.getGeometry());
  view.fit(polygon, {padding: [170, 50, 30, 150], nearest: true});
}, false);

var zoomtolausanne = document.getElementById('zoomtolausanne');
zoomtolausanne.addEventListener('click', function() {
  var feature = source.getFeatures()[1];
  var point = /** @type {module:ol/geom/SimpleGeometry~SimpleGeometry} */ (feature.getGeometry());
  view.fit(point, {padding: [170, 50, 30, 150], minResolution: 50});
}, false);

var centerlausanne = document.getElementById('centerlausanne');
centerlausanne.addEventListener('click', function() {
  var feature = source.getFeatures()[1];
  var point = /** @type {module:ol/geom/Point~Point} */ (feature.getGeometry());
  var size = /** @type {module:ol/size~Size} */ (map.getSize());
  view.centerOn(point.getCoordinates(), size, [570, 500]);
}, false);