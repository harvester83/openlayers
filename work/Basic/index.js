import Map from 'ol/Map';
import {Tile} from 'ol/layer';
import OSM from 'ol/source/OSM';
import {fromLonLat} from 'ol/proj';
import View from 'ol/View';

var map = new Map({
  target: 'map',
  layers: [
    new Tile({
      source: new OSM()
    })
  ],
  view: new View({
    center: fromLonLat([37.41, 8.82]),
    zoom: 4
  })
});