import Map from 'ol/Map';
import { Tile } from 'ol/layer';
import OSM from 'ol/source/OSM';
import { fromLonLat } from 'ol/proj';
import View from 'ol/View';

import Feature from 'ol/Feature';
import Polygon from 'ol/geom/Polygon';
import Point from 'ol/geom/Point';

const feature = new Feature({
  geometry: new Polygon([[[-5e6, 6e6], [-5e6, 8e6], [-3e6, 8e6], [-3e6, 6e6], [-5e6, 6e6]]]),
  labelPoint: new Point([30, 10]),
  name: 'My Polygon',
});

// get the polygon geometry
const poly = feature.getGeometry();

// Render the feature as a point using the coordinates from labelPoint
feature.setGeometryName('labelPoint');

// get the point geometry
const point = feature.getGeometry();

const map = new Map({
  target: 'map',
  layers: [
    new Tile({
      source: new OSM(),
    }),
  ],
  view: new View({
    center: fromLonLat([37.41, 8.82]),
    zoom: 4,
  }),
});
